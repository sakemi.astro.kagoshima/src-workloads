# Rucio-Dask-IVOA demo workflow

This is an example workflow to demonstrate an end-to-end workflow using data stored in the Rucio data lake prototype, discovered via IVOA-compliant metadata, and read by the workers on a (possibly remote) Dask cluster.

This workflow was built as a technical exercise to demonstrate the principle of reading Rucio-managed data directly from the Rucio Storage Element's Storage Resource Manager with remote Dask workers, and is not intended as an introduction to Dask. As such, this workflow is built for a more constrained execution environment than other workflows in this repository.

## Environment

This workflow takes the form of a Jupyter notebook. The notebook makes use of the Dask Gateway client, and is intended to be run on a Jupyter platform with a Dask Gateway deployment available (though it could be adapted to run on a local Dask cluster - see the `dask-rucio-ivoa-demo.ipynb` notebook).

The notebook has several package dependencies; these are installed in the Jupyter Notebook environment described by `jupyter/Dockerfile`. To run the full notebook also requires the Dask cluster to have some software packages, such as `astropy`, installed. `dask-gateway/Dockerfile` describes the environment needed for this aspect of the code. This is not intended for the end user to run; rather the Dask Gateway operator will be required to use this image for the notebook to run against their Dask Gateway platform.

## Running

The workflow is in the form of a Jupyter Notebook, which takes the user through the steps required to:
1. Obtain a URL to a data file stored in Rucio
2. Obtain an OIDC token
3. Start a Dask Gateway cluster
4. Define a Dask delayed function which loads the data from the storage
5. Compute the function as part of a plotting routine to display the data