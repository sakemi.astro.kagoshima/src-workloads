#!/bin/bash

function download_from_sdss_sas() {
    # Retrieve data from SDSS SAS
    echo "Downloading 4 fits files from SDSS SAS..."
    wget -nc -P data http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/6174/2/frame-r-006174-2-0094.fits.bz2
    wget -nc -P data http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/756/5/frame-r-000756-5-0595.fits.bz2
    wget -nc -P data http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/1233/5/frame-r-001233-5-0038.fits.bz2
    wget -nc -P data http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/1334/5/frame-r-001334-5-0056.fits.bz2
    
    # decompress files
    echo "Decompressing bz2 files..."
    cd data
    bzip2 -d *.bz2
    cd ../
}

# Download 4 images covering ARP 240
if [ -n "$DM_ACCESS_TOKEN" ]; then
    # Code to execute when DM_ACCESS_TOKEN is set
    echo "Variable DM_ACCESS_TOKEN set; downloading data from DM API..."
    python3 -c '
from astroquery.srcnet import SRCNet
import os
import sys

try:
    srcnet=SRCNet(access_token=os.environ["DM_ACCESS_TOKEN"]);
    for filename in [
        "frame-r-000756-5-0595.fits",
        "frame-r-001233-5-0038.fits",
        "frame-r-001334-5-0056.fits",
        "frame-r-006174-2-0094.fits"
    ]:
        print("Obtaining {}...".format(filename))
        srcnet.get_data(namespace="src-workloads", name=filename)
except Exception as e:
    print("Error encountered while downloading with Astroquery {}".format(e))
    sys.exit(1)
'
    if [ $? -ne 0 ]; then
        echo "Error occurred in Python script; falling back on SDSS SAS..."
        download_from_sdss_sas
    fi
else
    # Code to execute when DM_ACCESS_TOKEN is not set
    echo "DM_ACCESS_TOKEN is not set"
    download_from_sdss_sas
fi