#!/bin/bash
# get cpus
n=`nproc`
# Step2: De-dispersion
prepsubband -ncpus $n -lodm 1.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 3.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 5.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 7.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 9.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 11.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 13.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 15.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 17.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 19.000000 -nsub 32 -dmstep 0.020000 -numdms 100 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 21.000000 -nsub 32 -dmstep 0.020000 -numdms 93 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 22.870000 -nsub 32 -dmstep 0.050000 -numdms 100 -downsamp 2 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 27.870000 -nsub 32 -dmstep 0.050000 -numdms 100 -downsamp 2 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 32.870000 -nsub 32 -dmstep 0.050000 -numdms 100 -downsamp 2 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 37.870000 -nsub 32 -dmstep 0.050000 -numdms 100 -downsamp 2 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 42.870000 -nsub 32 -dmstep 0.050000 -numdms 57 -downsamp 2 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits
prepsubband -ncpus $n -lodm 45.740000 -nsub 32 -dmstep 0.110000 -numdms 100 -downsamp 4 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits