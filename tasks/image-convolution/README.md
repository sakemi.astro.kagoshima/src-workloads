# Image Gaussian convolution

[[_TOC_]]

## Description

Fundamentally, a little example to see how launching image processing functions in Dask cluster.
Concretely, LOFAR images are convolved with a Gaussian kernel. Two versions:

* **image_gauss_conv_multiprocessing.py** : using classical python module *Multiprocessing.py*

* **image_gauss_conv_dask.py**: using a *Dask cluster*

### <a name="parameters"></a> Input parameters

* **sigma**. Defining the Gaussian filter ((<span style="color:blue">optional</span>)). Default: (3,3)
* **dirIN**. Directory where the original images are stored ((<span style="color:blue">optional</span>)). Default: '/data'
* **dirOUT**. Directory where the convolved images are saved ((<span style="color:blue">optional</span>)). Default: '/data/output'
* **pattern**. pattern that fit the image name to convolve ((<span style="color:blue">optional</span>)). Default='*.fits' (all)
* **prefix**. prefix for the output convolved image name ((<span style="color:blue">optional</span>)). Default='conv_'

#### Specific to  workflow_convolution_multiprocessing:
* **ncpus**. Number of parallel processes used by Multiprocessing ((<span style="color:blue">optional</span>)). Default:0

#### Specific to workflow_convolution_dask:
* **ncores**. Number of cores used by Dask ((<span style="color:blue">optional</span>)). Default:0
* **ip_cluster**. Address of cluster with DASK ((<span style="color:blue">optional</span>)). Default: http://127.0.0.1:8888

### Ouput

* Set of convolved (fits) images saved in **dirOUT**


## How to run the task

Clone this repository and add execute permissions to files in the repository

    chmod 755 -R *

We use a Makefile to organise different ways to run this task, depending on your need. The Makefile shows all the available options, but to run it fully automatically the following command will pull the latest container, download the data, and run the task:

    make run-full

You can also use the Makefile to run things separately step by step, such as pulling the image (`make pull-image`), downloading the data (`make get-data`), and running the source-finding (`make run-task`).

If you has available a cluster with Dask you can run:

    make run-full-dask
    make run-task-dask

You must specify the cluster IP, see examples below, if it is not locally.


## Directory structure

<pre>
├──jupyter
│       │
│       ├── image_gauss_conv_dask
│       │      │
│       │      ├── scripts
│       │      │        │
│       │      │        ├── image_gauss_conv_dask.ipynb
│       │      │        │
│       │      │        └── image.fits
│       │      │
│       │      └── Dockerfile
│       │
│       └── image_gauss_conv_multiprocessing
│             │
│             ├── scripts
│             │         │
│             │         ├── image_gauss_conv_multiprocessing.ipynb
│             │         │
│             │         └── image.fits
│             │
│             └── Dockerfile
│
├── scripts
│       │
│       ├── get-data.sh
│       │
│       ├── image_gauss_conv_dask.py
│       │
│       ├── image_gauss_conv_dask.sh
│       │
│       ├── image_gauss_conv_multiprocessing.py
│       │
│       └── image_gauss_conv.sh
│
├── Dockerfile
│
├── Makefile
│
├── README.md
│
└── requeriments.txt
</pre>


## Dependencies
* scikit-image
* dask==2022.12.1
* distributed==2022.12.1
* dask-gateway==2023.1.1
* astropy==5.2.2


##  Usage (see [parameters](#parameters))

### For image_gauss_conv_multiprocessing:
    ```
    $ ./image_gauss_conv_multiprocessing.py --help
    usage: image_gauss_conv_multiprocessing.py [-h] [--ncpus NCPUS] [--dirIN DIRIN] [--dirOUT DIROUT] [--pattern PATTERN] [--prefix PREFIX]

    image processing

    options:
    -h, --help         show this help message and exit
    --ncpus NCPUS      Number of cpus to use (default:1))
    --dirIN DIRIN      dir where the images lay
    --dirOUT DIROUT    dir where the convolved images are saved
    --pattern PATTERN  name pattern of the images to process
    --prefix PREFIX    name prefix of the convolved images
    ```
#### Examples

  * `./image_gauss_conv_multiprocessing.py`

  * `./image_gauss_conv_multiprocessing.py --ncpus 2 --dirIN . --dirOUT conv --pattern '*.fits' --prefix conv_`


### For image_gauss_conv_dask:
    ```
     $ ./image_gauss_conv_dask.py --help
    usage: image_gauss_conv_multiprocessing.py [-h] [--ncores NCORES] [--ip_cluster IP_CLUSTER][--dirIN DIRIN] [--dirOUT DIROUT] [--pattern PATTERN] [--prefix PREFIX]

    image processing

    options:
    -h, --help         show this help message and exit
    --ncpus NCORES     Number of cores to use (default:1))
    --ip_cluster       Cluster IP with DASK
    --dirIN DIRIN      dir where the images lay
    --dirOUT DIROUT    dir where the convolved images are saved
    --pattern PATTERN  name pattern of the images to process
    --prefix PREFIX    name prefix of the convolved images
    ```

    ```
#### Examples

  * `./image_gauss_conv_dask.py`

  * `./image_gauss_conv_dask.py --ip_cluster http://127.0.0.1:8888' --ncores 2 --dirIN . --dirOUT conv --pattern '*.fits' --prefix conv_`


## How to use a Jupyter notebook

The Makefile also includes options for using Jupyter notebooks.

    make build-image-jupyter
    make run-local-jupyter

then a link will appear which you can copy into your browser to access. You can then start a terminal to run things with the Makefile, or you can load the Jupyter notebook in the scripts directory.


Set the [*INPUT* parameters](#parameters)
    * sigma
    * dirIN
    * dirOUT
    * pattern
    * prefix
    * ncpus (workflow_convolution_multiprocessing)
      or
      ncores & ip_cluster (workflow_convolution_dask)
