import json
import urllib.request
import os, sys
import click

@click.group()
def cli():
        pass
    

input_json = "data.json"
output_dir = sys.argv[1]


# Open the JSON file
with open(input_json, 'r') as infile:
    data = json.load(infile)

images = []
for item in data:
    for field in item[1]:
        if field not in images:
            images.append(field)

for image in images:
    # Extract the filename from the URL
    filename = os.path.basename(image)
    filename = urllib.parse.unquote(filename)+'.fits'

    # Set the complete destination path
    destination = os.path.join(output_dir, filename)
    
    # Check if already downloaded
    if os.path.exists(destination):
        print(f"{destination} already exists, not downloading")
    else:
        print(f"Downloading {filename}")
        urllib.request.urlretrieve(image, destination)
