#!/bin/bash

# download to the /data folder since entrypoint is /data
wget -nc --cut-dirs=5 -P data https://vo.astron.nl/getproduct/hetdex/data/low-mosaics/P21-low-mosaic.fits
